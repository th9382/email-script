import smtplib, os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import getpass
from os.path import isfile, join
import time


gmail_user = str(raw_input('email: '))
gmail_pwd = getpass.getpass("Password: ")

# specify month
c_month = "August"

# path of commission sheets
mypath = '/Users/th/'

send_where = raw_input("Send to 'reps' or 'payroll'?: ")

# Build dictionary of recipients: includes Full name, role, email, and manager
employees = {'First Last': {'role': 'VP', 'email': 'email1@example.com'},
    'First2 Last 2': {'role': 'Dir', 'email': 'email2@example.com', 'manager': 'First Last'},
}

# get all files in folder 
onlyfiles = [ f for f in os.listdir(mypath) if isfile(join(mypath,f)) ]

# get list of commissions files (only xlsx files)
files = []
for f in onlyfiles:
    if f[-4:] == 'xlsx':
            files.append(f)

# Build list of files that includes the rep names, extracted from file names. File names follow a standard format 'First Last - FY2015.xlsx'
reps = []
for rep in files:
   reps.append(rep[:rep.find(' - ')])

print files
print "-" * 50
print reps
print "-" * 50
print c_month


go_no_go = raw_input("Does this look correct? Type 'yes' to proceed: ")
print go_no_go

if go_no_go != "yes":
    print "Stopped!"
    raise SystemExit(0)


# function to send emails
def send_mail(send_from, send_to, subject, text, path, files=[]):
    assert type(send_to)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(path+f,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
        msg.attach(part)


    smtp = smtplib.SMTP("smtp.gmail.com",587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo
    smtp.login(gmail_user, gmail_pwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def main():
    # Send all emails to reps
    try:
        for rep in reps:
            rep_fname = rep[:rep.find(" ")]
            rep_lname = rep[rep.find(" ")+1:]
            rep_manager = employees[rep]['manager']
            rep_email = employees[rep]['email']
            manager_fname = employees[rep]['manager'][:employees[rep]['manager'].find(" ")]
            manager_email = employees[rep_manager]['email']
            email_list = [rep_email, manager_email, gmail_user]
            rep_file = ['%s - FY2015.xlsx' % rep]
            email_subject = '%s - Commission Statement %s 2014' % (rep_lname, c_month)
            email_text = ' %s, \n \n Attached is your preliminary Commission Statement for %s. \n \n Please review with %s by EOD Friday. \n \n Make sure you download the file to view the correct tab in the spreadsheet. \n \n Thanks, \n \n Me' % (rep_fname, c_month, manager_fname)
            send_mail(gmail_user, email_list,email_subject, email_text, mypath, rep_file)
            print 'Email sent to %s (%s), %s (%s). File name %s.' % (rep_lname, rep_email, rep_manager, manager_email, rep_file)
            time.sleep(2)
    except KeyError:
        print "error in hash table"

# Send summary file to Payroll email
def payroll():
        email_list = ["First_last@example.com","payroll@example.com"]
        rep_file = ['EAE_Comp_Summary.xlsm']
        email_subject = 'EAE - Commission Summary %s 2014' % c_month
        email_text = 'Here are the EAE Commissions for %s. \n \n \n Me' % c_month
        send_mail(gmail_user, email_list,email_subject, email_text, '/Users/th/other/',rep_file)
        print 'Email sent to payroll'


# Execute Reps or Payroll?
if send_where == "reps":
    main()
elif send_where == "payroll":
    payroll()
else:
    pass


